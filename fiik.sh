#!/bin/bash
# Fucked if I know
# CLI 4chan image downloader
# by radix
###############################

TMP_DIR="/tmp/fiik"
OUT_DIR="$2"
DEBUG="0"

if [ -z "$1" ]; then
	echo "Usage: $0 <thread url> [directory]"
	exit 1
fi

if [ ! -d "images" ]; then
	mkdir images
fi

if [ -d ${TMP_DIR} ]; then
  echo "looks like tmp dir exists, cleaning it out"
  rm -rf ${TMP_DIR}
fi

if [ ! -d ${TMP_DIR} ]; then
  mkdir -p ${TMP_DIR}
fi

counter=0

echo "grabbing all images from thread $1"

wget -q $1 -P ${TMP_DIR}

while [ $? -ne "8" ]; do

  i=$(echo $1 | cut -d '/' -f 6)
  
  #egrep -Eo 'is2.4chan.org/[^"]{1,3}/[^"]+' ${TMP_DIR}/$i | uniq | sed -e 's/is2\.4chan\.org/i\.4cdn\.org/g' > ${TMP_DIR}/image_$i;

  #egrep -Eo 'i.4cdn.org/[^"]{1,3}/[^"]+' ${TMP_DIR}/$i | uniq | sed -e 's/is2\.4chan\.org/i\.4cdn\.org/g' | grep -v s.jpg > ${TMP_DIR}/image_$i;

  egrep -Eo 'is2.4chan.org/[^"]{1,3}/[^"]+' ${TMP_DIR}/$i | uniq | grep -v s.jpg > ${TMP_DIR}/image_$i;

    if [ -z "${OUT_DIR}" ]; then
      OUT_DIR="${i}"
    fi

    if [ ! -d "images/${OUT_DIR}" ]; then
      mkdir -p images/${OUT_DIR}
    fi

  for post in $(cat ${TMP_DIR}/image_$i); do
    j=$(echo $post | cut -d '/' -f 3)

      if [ -f "images/${OUT_DIR}/$j" ]; then
        continue
      else
        echo "Grabbing image $j"

      if [ ${DEBUG} == "1" ]; then
        echo "Debug enabled:"
        wget -nc --unlink https://$post -P images/${OUT_DIR}/;
      else
        wget -q -nc --unlink https://$post -P images/${OUT_DIR}/;
      fi

        counter=`wc -l ${TMP_DIR}/image_$i | cut -d ' ' -f 1`
      fi
    done;

  rm ${TMP_DIR}/$i

  wget -q $1 -P ${TMP_DIR}

  dead=$(egrep -Eo '<div class="closed">Thread archived.<br>You cannot reply anymore.</div>' ${TMP_DIR}/$i)

  #if [ $? -eq "8" ]; then
  if [ ! -z "$dead" ] ; then

  cat<<EOF

444   444 000000000 444   444
444   444 000   000 444   444
444   444 000   000 444   444  
444444444 000   000 444444444
444444444 000   000 444444444
      444 000   000       444
      444 000   000       444  $counter images downloaded from
      444 000000000       444  thread $i
EOF

      exit 0
    fi

  echo "Grabbed $counter images so far, Sleeping 15 seconds"

  sleep 15

done;

echo "Thread 404'd or url is invalid, cleaning up"
rm -rf ${TMP_DIR}
